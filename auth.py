from flask import abort, request
import os
from msal.oauth2cli.oidc import decode_id_token


def azure_authenticated(roles):
    def wrap(f):
        def decorated(*args, **kwargs):
            azure_ad_token = request.headers.get('authorization')
            if azure_ad_token:
                try:
                    decoded_token = decode_id_token(id_token=azure_ad_token.split()[1], client_id=f"api://{os.environ.get('AZURE_APP_ID')}")
                except Exception:
                    pass
                else:
                    if roles and len(roles) > 0:
                        for role in roles:
                            if role in decoded_token["roles"]:
                                return f(*args, **kwargs)

                        abort(
                            401,
                            {
                                "error": "missing_role",
                                "description": "Access denied."
                            }
                        )
                    else:
                        return f(*args, **kwargs)

            if os.environ.get("FLASK_ENV") == "test":
                return f(*args, **kwargs)

            abort(
                401,
                {
                    "error": "bad_auth",
                    "description": "Authentication required."
                }
            )
        return decorated
    return wrap
