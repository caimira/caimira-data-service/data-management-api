import os
from datetime import timedelta
from pathlib import Path

CUR_DIR = Path(__file__).parent.absolute()

_MAX_UPDATE_RETRIES = 10


def _env_bool(name, default):
    val = os.environ.get(name)
    if val:
        return val.lower() == "true"
    return default


def _env_int(name, default):
    val = os.environ.get(name, "_" + name)
    try:
        return int(val)
    except Exception:
        return default


class Config:
    """Base configuration class."""

    # --------------------------------------------------------------------------
    # Flask builtin configuration parameters
    # https://flask.palletsprojects.com/en/2.2.x/config/
    # --------------------------------------------------------------------------
    DEBUG = _env_bool("DEBUG", False)
    TESTING = _env_bool("TESTING", False)
    SECRET_KEY = os.environ.get("SECRET_KEY")

    # --------------------------------------------------------------------------
    # flask-jwt-extended configuration parameters
    # https://flask-jwt-extended.readthedocs.io/en/stable/options/
    # --------------------------------------------------------------------------
    JWT_TOKEN_LOCATION = ["headers", "cookies"]
    JWT_ACCESS_TOKEN_EXPIRES = timedelta(days=7)
    JWT_COOKIE_SECURE = _env_bool("JWT_COOKIE_SECURE", True)

    # --------------------------------------------------------------------------
    # flask-sqlalchemy configuration parameters
    # https://flask-sqlalchemy.palletsprojects.com/en/2.x/config/
    # --------------------------------------------------------------------------
    SQLALCHEMY_TRACK_MODIFICATIONS = _env_bool("SQLALCHEMY_TRACK_MODIFICATIONS", False)
    SQLALCHEMY_DATABASE_URI = os.environ.get("SQLALCHEMY_DATABASE_URI")

    # --------------------------------------------------------------------------
    # Alembic configuration
    # --------------------------------------------------------------------------
    ALEMBIC = {"script_location": str(CUR_DIR / "migrations")}

    MAX_UPDATE_RETRIES = _MAX_UPDATE_RETRIES


class DevelopmentConfig(Config):
    """
    Configuration used when the environment variable FLASK_ENV is "dev".
    """
    DEBUG = True
    SQLALCHEMY_DATABASE_URI = "sqlite:///tests/test.db"
    SQLALCHEMY_TRACK_MODIFICATIONS = True


class TestConfig(Config):
    """
    Configuration used when the environment variable FLASK_ENV is "test".
    """

    DEBUG = True
    TESTING = True
    SECRET_KEY = "test-key"

    JWT_COOKIE_SECURE = False

    SQLALCHEMY_DATABASE_URI = "sqlite:///tests/test.db"
    SQLALCHEMY_TRACK_MODIFICATIONS = True
