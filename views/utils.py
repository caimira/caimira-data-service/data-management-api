from models import DataSchema
from jsonschema import validate

frequencies = [
    "hourly",
    "daily",
    "weekly",
    "monthly"
]


def replace_value(json_blob, path, value):
    """Replaces the a value of the indicated path (path.to.key)"""
    keys = path.split(".")
    ref = json_blob

    if len(keys) > 1:
        # Update the reference until its just one above the value that needs to be changed
        for key in keys[:1]:

            if key not in ref:
                ref[key] = {}
            ref = ref[key]

        if keys[-1] in ref and ref[keys[-1]] == value:
            return False

        ref[keys[-1]] = value
    else:
        if path in ref and ref[path] == value:
            return False

        ref[path] = value

    return True


def apply_changes(json_blob, changes):
    """Replaces the a value of the json_blob with the values in the changes dictionary"""
    for key, value in changes.items():
        replace_value(json_blob, key, value)
    return json_blob


def dict_difference(dict1, dict2, parent_key=''):
    removed = [
        {
            'key': f'{parent_key}.{key}' if parent_key else key,
            'value': value
        } for key, value in dict1.items() if key not in dict2
    ]

    added = [
        {
            'key': f'{parent_key}.{key}' if parent_key else key,
            'value': value
        } for key, value in dict2.items() if key not in dict1
    ]

    updated = []

    for key in set(dict1) & set(dict2):
        current_key = f'{parent_key}.{key}' if parent_key else key

        if isinstance(dict1[key], dict) and isinstance(dict2[key], dict):
            nested_difference = dict_difference(dict1[key], dict2[key], current_key)
            removed.extend(nested_difference['removed'])
            added.extend(nested_difference['added'])
            updated.extend(nested_difference['updated'])
        elif dict1[key] != dict2[key]:
            updated.append({
                'key': current_key,
                'old_value': dict1[key],
                'new_value': dict2[key]
            })

    difference = {
        'removed': removed,
        'added': added,
        'updated': updated
    }

    return difference


class Validator():
    def __init__(self):
        self.schema = DataSchema.query.order_by(DataSchema.version.desc()).all()

    def validate(self, json_blob):
        validate(instance=json_blob, schema=self.schema[0].json_schema)
