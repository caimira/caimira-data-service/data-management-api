from flask import Blueprint, abort, current_app
from flask_apispec import MethodResource, marshal_with, use_kwargs

from sqlalchemy import exc
from marshmallow import fields
from jsonschema import validate

from auth import azure_authenticated
from extensions import apispec
from models import db, Data, DataSchema
from schemas import DataSchema as data_schema
from .utils import replace_value, apply_changes, dict_difference, Validator

#
# REST API views
#
api = Blueprint("data", __name__)


def get_latest_data():
    data = Data.query.order_by(Data.version.desc()).all()
    if len(data) == 0:
        return Data(json_blob={}, version=0, schema_version=0, reason="Initial")
    else:
        return data[0]


def get_latest_dataschema():
    schema = DataSchema.query.order_by(DataSchema.version.desc()).all()
    if len(schema) == 0:
        return DataSchema(json_schema={}, version=0, reason="Initial")
    else:
        return schema[0]


def commit_update(json_data, path):
    data = Data(json_blob=json_data.json_blob, version=json_data.version + 1, schema_version=json_data.schema_version, reason=f"{path} updater")
    db.session.add(data)
    db.session.commit()

    return data.json_blob


class PingResource(MethodResource):
    @azure_authenticated([])
    def get(self):
        return "Pong"


class DataSchemaVersionResource(MethodResource):
    def get(self, version):
        """Get the json schema by version
        ---
        get:
          responses:
            200:
              content:
                application/json:
                  schema: DataSchema
        """
        schema = DataSchema.query.filter_by(version=version).order_by(DataSchema.version.desc()).all()
        if len(schema) == 0:
            abort(
                409,
                {
                    "error": "schema_version_not_found",
                    "description": "Could not find the requested schema version."
                }
            )
        else:
            schema = schema[0]

        return {
            "json_schema": schema.json_schema,
            "version": schema.version
        }


class DataSchemaResource(MethodResource):
    def get(self):
        """Get the latest json schema"""
        schema = get_latest_dataschema()

        return {
            "json_schema": schema.json_schema,
            "version": schema.version
        }

    @azure_authenticated(["admin", "expert"])
    @marshal_with(data_schema)
    @use_kwargs({"json_schema": fields.Dict(), "json_blob": fields.Dict(), "reason": fields.Str()})
    def put(self, **kwargs):
        """Change the data schema"""
        old_schema = get_latest_dataschema()

        try:
            validate(instance=kwargs["json_blob"], schema=kwargs["json_schema"])
        except Exception as e:
            abort(
                409,
                {
                    "error": "schema_error",
                    "description": f"JSON data does not match the schema: {e}"
                }
            )
        else:
            new_version = old_schema.version + 1
            schema = DataSchema(version=new_version, json_schema=kwargs["json_schema"], reason=kwargs["reason"])
            data = Data(
                version=get_latest_data().version + 1,
                schema_version=new_version,
                json_blob=kwargs["json_blob"],
                reason="JSON schema update"
            )

            db.session.add(schema)
            db.session.add(data)
            db.session.commit()


class DataVersionResource(MethodResource):
    def get(self, version):
        """Get the data with specified version"""
        data = Data.query.filter_by(version=version).order_by(Data.version.desc()).all()
        if len(data) == 0:
            abort(
                409,
                {
                    "error": "version_not_found",
                    "description": "Could not find the requested version."
                }
            )
        else:
            data = data[0]

        return {
            "data": data.json_blob,
            "version": data.version,
            "schema_version": data.schema_version
        }


class DataVersionSchemaResource(MethodResource):
    def get(self, version):
        """Get the latest data with specified schema version"""
        data = Data.query.filter_by(schema_version=version).order_by(Data.version.desc()).all()
        if len(data) == 0:
            abort(
                409,
                {
                    "error": "schema_version_not_found",
                    "description": "Could not find the requested schema version."
                }
            )
        else:
            data = data[0]

        return {
            "data": data.json_blob,
            "version": data.version,
            "schema_version": data.schema_version
        }


class DataHistoryResource(MethodResource):
    def get(self, page_size, page_number):
        """Get the data for the history page"""
        data = Data.query.order_by(Data.version.desc()).offset(page_number * page_size).limit(page_size + 1).all()
        if len(data) <= 1:
            return {}

        history = []

        for i in range(len(data) - 1):
            diff = dict_difference(data[i + 1].json_blob, data[i].json_blob)
            diff["version"] = data[i].version
            diff["schema_version"] = data[i].schema_version
            diff["reason"] = data[i].reason

            history.append(diff)

        return {
            "history": history,
            "pages": (((Data.query.count() - 1) // page_size) + 1)
        }


class DataResource(MethodResource):
    def get(self):
        """Get the latest data"""
        data = get_latest_data()

        return {
            "data": data.json_blob,
            "version": data.version,
            "schema_version": data.schema_version
        }

    @azure_authenticated(["admin", "expert"])
    @marshal_with(data_schema)
    @use_kwargs(data_schema(only={"json_blob", "reason"}))
    def put(self, **kwargs):
        """Change the json_blob"""
        old_data = get_latest_data()
        data = Data(schema_version=old_data.schema_version, version=old_data.version + 1, **kwargs)

        validator = Validator()
        if old_data.schema_version != validator.schema[0].version:
            abort(
                409,
                {
                    "error": "mismatched_schema",
                    "description": "Latest JSON schema version is not the same as the one used for the old data"
                }
            )
        else:
            try:
                validator.validate(data.json_blob)
            except Exception as e:
                abort(
                    409,
                    {
                        "error": e,
                        "description": "JSON data does not match the schema"
                    }
                )
            else:
                db.session.add(data)
                db.session.commit()


class DataResetResource(MethodResource):
    @azure_authenticated(["admin", "expert"])
    def post(self):
        """Resets the version of the data object"""
        old_schema = get_latest_dataschema()
        data = Data.query.filter_by(schema_version=old_schema.version).order_by(Data.version.desc()).all()
        if len(data) > 1:
            data = data[-1]
        else:
            abort(
                409,
                {
                    "error": "latest_version",
                    "description": "The current JSON data is the latest"
                }
            )

        old_data = get_latest_data()
        data.version = old_data.version + 1

        db.session.add(data)
        db.session.commit()

        return {
            "data": data.json_blob,
            "version": data.version,
            "schema_version": data.schema_version
        }


class DataResetVersionResource(MethodResource):
    @azure_authenticated(["admin", "expert"])
    def post(self, version):
        """Resets the version of the data object"""
        data = Data.query.filter_by(schema_version=version).order_by(Data.version.desc()).all()
        if len(data) > 0:
            data = data[-1]
        else:
            abort(
                409,
                {
                    "error": "invalid_version",
                    "description": f"There is no version {version} of the JSON data"
                }
            )

        old_data = get_latest_data()
        if data.schema_version != old_data.schema_version:
            old_schema = DataSchema.query.filter_by(version=data.schema_version).order_by(DataSchema.version.desc()).all()[0]

            old_schema.version = old_data.schema_version + 1
            db.session.add(old_schema)
            db.session.commit()

            data.schema_version = old_data.schema_version + 1

        data.version = old_data.version + 1

        db.session.add(data)
        db.session.commit()

        return {
            "data": data.json_blob,
            "version": data.version,
            "schema_version": data.schema_version
        }


class UpdateResource(MethodResource):
    @azure_authenticated(["admin", "updater"])
    @use_kwargs({"data": fields.Raw()})
    def post(self, path, **kwargs):
        """Update values in the json_blob of the specified path"""
        data = get_latest_data()

        validator = Validator()

        changes = {}
        backup = dict(data.json_blob)
        if replace_value(data.json_blob, path, kwargs["data"]):
            try:
                validator.validate(data.json_blob)
            except:
                data.json_blob = backup
                abort(
                    409,
                    {
                        "error": "failed_schema_validation",
                        "description": f"Updated data does not pase schema validation for path: {path}"
                    }
                )

            else:
                changes[path] = kwargs["data"]

        if len(changes) > 0:
            MAX_ATTEMPTS = current_app.config["MAX_UPDATE_RETRIES"]
            for i in range(MAX_ATTEMPTS):
                try:
                    json_blob = commit_update(data, path)
                    return json_blob
                except exc.IntegrityError:
                    db.session.rollback()
                    data = get_latest_data()
                    apply_changes(data.json_blob, changes)
            raise f"Failed to update DB in {MAX_ATTEMPTS} retries."


api.add_url_rule(
    "/ping",
    view_func=PingResource.as_view("ping"),
)
api.add_url_rule(
    "/schema",
    view_func=DataSchemaResource.as_view("schema"),
)
api.add_url_rule(
    "/schema/version/<int:version>",
    view_func=DataSchemaVersionResource.as_view("schema_version"),
)
api.add_url_rule(
    "/data",
    view_func=DataResource.as_view("data"),
)
api.add_url_rule(
    "/data/version/<int:version>",
    view_func=DataVersionResource.as_view("data_version"),
)
api.add_url_rule(
    "/data/schema_version/<int:version>",
    view_func=DataVersionSchemaResource.as_view("data_schema_version"),
)
api.add_url_rule(
    "/data/history/<int:page_size>/<int:page_number>",
    view_func=DataHistoryResource.as_view("data_history"),
)
api.add_url_rule(
    "/data/reset",
    view_func=DataResetResource.as_view("data_reset"),
)
api.add_url_rule(
    "/data/reset/<int:version>",
    view_func=DataResetVersionResource.as_view("data_reset_version"),
)
api.add_url_rule(
    "/update/<string:path>",
    view_func=UpdateResource.as_view("update"),
)

apispec.register(PingResource, "data.ping")
apispec.register(DataSchemaResource, "data.schema")
apispec.register(DataSchemaVersionResource, "data.schema_version")
apispec.register(DataVersionResource, "data.data_version")
apispec.register(DataVersionSchemaResource, "data.data_schema_version")
apispec.register(DataHistoryResource, "data.data_history")
apispec.register(DataResetResource, "data.data_reset")
apispec.register(DataResetVersionResource, "data.data_reset_version")
apispec.register(DataResource, "data.data")
apispec.register(UpdateResource, "data.update")
