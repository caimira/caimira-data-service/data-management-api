from flask_alembic import Alembic
from flask_apispec import FlaskApiSpec

alembic = Alembic()
apispec = FlaskApiSpec()
