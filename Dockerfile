FROM python:3.10

WORKDIR /app
COPY requirements.txt ./
RUN pip install -r requirements.txt
COPY ./views ./views
COPY ./models ./models
COPY ./schemas ./schemas
COPY app.py auth.py extensions.py config.py dev.py uwsgi.ini pytest.ini ./

ENV FLASK_ENV poduction
EXPOSE 5000
CMD ["uwsgi", "--ini", "uwsgi.ini"]