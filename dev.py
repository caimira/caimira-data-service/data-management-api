def create_dev_db(app, db):
    with app.app_context():
        from models import Data, DataSchema
        db.create_all()

        schema_versions = db.session.query(DataSchema).count()
        if schema_versions == 0:
            json_schema = {
                "type": "object",
                "properties": {
                    "foo": {"type": "string"},
                    "noon": {"type": "integer"}
                }
            }
            dataRow = DataSchema(version=0, json_schema=json_schema, reason="Initial")
            db.session.add(dataRow)
            db.session.commit()

        versions = db.session.query(Data).count()
        if versions == 0:
            json_blob = {
                "foo": "bar",
                "noon": 0
            }
            dataRow = Data(version=0, schema_version=0, json_blob=json_blob, reason="Initial")
            db.session.add(dataRow)
            db.session.commit()

        db.session.commit()
