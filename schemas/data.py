from marshmallow import Schema, fields


class DataSchema(Schema):

    id = fields.String()
    version = fields.Integer()
    schema_version = fields.Integer()
    json_blob = fields.Dict()
    reason = fields.String()
