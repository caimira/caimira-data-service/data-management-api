from marshmallow import Schema, fields


class DataSchemaSchema(Schema):

    id = fields.String()
    version = fields.Integer()
    json_schema = fields.Dict()
    reason = fields.String()
