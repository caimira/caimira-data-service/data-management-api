import os
from setuptools import setup


def read_requirements_file(fname):
    with open(fname) as f:
        return [
            dep
            for d in f.readlines()
            if (dep := d.strip()) and not (dep.startswith(("-", "#")) or "://" in dep)
        ]


def get_requirements(fname):
    return read_requirements_file(os.path.join(os.path.dirname(__file__), fname))


setup(
    install_requires=get_requirements("requirements.txt")
)
