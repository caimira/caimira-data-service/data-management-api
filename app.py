import os
from flask import Flask
from flask_cors import CORS
from flask_migrate import Migrate

import extensions
import config
from models import db
from views import data


def create_app():
    app = Flask(__name__)
    CORS(app, origins=[str(os.environ.get("FRONTEND_ADDRESS"))], supports_credentials=True)

    if app.env == "test":
        cfg = config.TestConfig
    elif app.env == "dev":
        cfg = config.DevelopmentConfig
    else:
        cfg = config.Config

    app.config.from_object(cfg)
    db.init_app(app)
    extensions.alembic.init_app(app, run_mkdir=False, command_name="alembic")
    Migrate(app, db)

    app.register_blueprint(data.api)
    extensions.apispec.init_app(app)

    if app.env == "dev":
        from dev import create_dev_db
        create_dev_db(app, db)

    return app


app = create_app()
