from views import utils


def test_ReplaceValue_SinglePath(test_data):
    utils.replace_value(test_data, "path", "two")
    assert test_data["path"] == "two"


def test_ReplaceValue_DoublePath(test_data):
    utils.replace_value(test_data, "other.inside", 1)
    assert test_data["other"]["inside"] == 1


def test_ReplaceValue_DifferentType(test_data):
    utils.replace_value(test_data, "other.paths", 0)
    assert test_data["other"]["paths"] == 0


def test_ReplaceValue_Dictionary_Empty(test_data):
    utils.replace_value(test_data, "dict", {})
    assert test_data["dict"] == {}


def test_ReplaceValue_Dictionary_Replace(test_data):
    expected = {
        "one": 2,
        "two": 2
    }
    utils.replace_value(test_data, "dict", expected)
    assert test_data["dict"] == expected


def test_ReplaceValue_Dictionary_Add(test_data):
    expected = {
        "one": 1,
        "two": 2,
        "three": 3
    }
    utils.replace_value(test_data, "dict", expected)
    assert test_data["dict"] == expected


def test_ReplaceValue_NonExistingKey_SinglePath(test_data):
    expected = "exists"
    utils.replace_value(test_data, "nil", expected)
    assert test_data["nil"] == expected


def test_ReplaceValue_NonExistingKey_MultiplePath(test_data):
    expected = "exists"
    utils.replace_value(test_data, "nil.indeed", expected)
    assert test_data["nil"]["indeed"] == expected


def test_ReplaceValue_ExistingKey_SameValue(test_data):
    assert not utils.replace_value(test_data, "path", test_data["path"])


def test_ReplaceValue_ExistingKey_SameValue_MultiplePath(test_data):
    assert not utils.replace_value(test_data, "other.inside", test_data["other"]["inside"])


def test_Update_Data(test_data):
    changes = {
        "other.paths": 3
    }
    utils.apply_changes(test_data, changes)
    new_blob = {
        "path": "one",
        "other": {
            "inside": 0,
            "paths": 3
        },
        "dict": {
            "one": 1,
            "two": 2
        },
        "array": ["one", "two"]
    }
    assert test_data == new_blob


def test_Difference_NoDifference():
    dict1 = {
        "foo": "bar"
    }
    assert utils.dict_difference(dict1, dict1) == {
        "removed": [],
        "added": [],
        "updated": []
    }


def test_Difference_ChangeSingleKey_SameType():
    dict1 = {
        "foo": "bar"
    }
    dict2 = {
        "foo": "roh"
    }
    assert utils.dict_difference(dict1, dict2) == {
        "removed": [],
        "added": [],
        "updated": [{"key": "foo", "old_value": "bar", "new_value": "roh"}]
    }


def test_Difference_ChangeSingleKey_DifferentType():
    dict1 = {
        "foo": "bar"
    }
    dict2 = {
        "foo": 0
    }
    assert utils.dict_difference(dict1, dict2) == {
        "removed": [],
        "added": [],
        "updated": [{"key": "foo", "old_value": "bar", "new_value": 0}]
    }


def test_Difference_ChangeMultipleKeys():
    dict1 = {
        "foo": "bar",
        "lorum": "ipsum"
    }
    dict2 = {
        "foo": "ipsum",
        "lorum": "bar"
    }
    expected_difference = {
        "removed": [],
        "added": [],
        "updated": [
            {"key": "foo", "old_value": "bar", "new_value": "ipsum"},
            {"key": "lorum", "old_value": "ipsum", "new_value": "bar"}
        ]
    }
    actual_difference = utils.dict_difference(dict1, dict2)
    # Sort the 'updated' list in both the expected and actual outputs
    expected_difference["updated"] = sorted(expected_difference["updated"], key=lambda x: x["key"])
    actual_difference["updated"] = sorted(actual_difference["updated"], key=lambda x: x["key"])
    assert actual_difference == expected_difference


def test_Difference_AddSingleKey():
    dict1 = {
        "foo": "bar",
    }
    dict2 = {
        "foo": "bar",
        "lorum": "ipsum"
    }
    assert utils.dict_difference(dict1, dict2) == {
        "removed": [],
        "added": [{"key": "lorum", "value": "ipsum"}],
        "updated": []
    }


def test_Difference_AddMultipleKeys():
    dict1 = {
        "foo": "bar",
    }
    dict2 = {
        "foo": "bar",
        "lorum": "ipsum",
        "test": "test"
    }
    assert utils.dict_difference(dict1, dict2) == {
        "removed": [],
        "added": [
            {"key": "lorum", "value": "ipsum"},
            {"key": "test", "value": "test"}
        ],
        "updated": []
    }


def test_Difference_RemoveSingleKey():
    dict1 = {
        "foo": "bar",
    }
    dict2 = {
        "foo": "bar",
        "lorum": "ipsum"
    }
    assert utils.dict_difference(dict2, dict1) == {
        "removed": [{"key": "lorum", "value": "ipsum"}],
        "added": [],
        "updated": []
    }


def test_Difference_All():
    dict1 = {
        "foo": "bar",
        "lorum": "ipsum",
        "noon": 0,
    }
    dict2 = {
        "foo": "bar",
        "test": "test",
        "noon": 1
    }
    assert utils.dict_difference(dict1, dict2) == {
        "removed": [
            {"key": "lorum", "value": "ipsum"}
        ],
        "added": [
            {"key": "test", "value": "test"}
        ],
        "updated": [
            {"key": "noon", "old_value": 0, "new_value": 1}
        ]
    }


def test_Difference_Dict_Add():
    dict1 = {
        "foo": "bar"
    }
    dict2 = {
        "foo": "bar",
        "test": {"test": "test"}
    }
    assert utils.dict_difference(dict1, dict2) == {
        "removed": [],
        "added": [
            {"key": "test", "value": {"test": "test"}}
        ],
        "updated": []
    }


def test_Difference_Dict_Remove():
    dict1 = {
        "foo": "bar"
    }
    dict2 = {
        "foo": "bar",
        "test": {"test": "test"}
    }
    assert utils.dict_difference(dict2, dict1) == {
        "removed": [
            {"key": "test", "value": {"test": "test"}}
        ],
        "added": [],
        "updated": []
    }


def test_Difference_Dict_Update():
    dict1 = {
        "foo": "bar",
        "test": {"test": "test"}
    }
    dict2 = {
        "foo": "bar",
        "test": {"test": "testing"}
    }
    assert utils.dict_difference(dict1, dict2) == {
        "removed": [],
        "added": [],
        "updated": [
            {"key": "test.test", "old_value": "test", "new_value": "testing"}
        ]
    }


def test_Validate(data1_data, data_schema1):
    validator = utils.Validator()
    try:
        validator.validate(data1_data["json_blob"])
    except Exception as e:
        assert False, e.message
