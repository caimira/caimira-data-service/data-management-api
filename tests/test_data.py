from types import SimpleNamespace
import json
from models import Data, DataSchema
from views import data as d


def wrong_status_response_log(expected_code, response):
    return f"Status code {expected_code} expected, got: {response.status_code}\nResponse log: {json.dumps(response.get_json(), indent=4, sort_keys=True)}"


def test_GetPing(db, client):
    response = client.get("/ping")
    assert response.status_code == 200, wrong_status_response_log(200, response)
    assert json.loads(response.data) == "Pong"


def test_GetSchema_WithoutData(db, client):
    response = client.get("/schema")
    assert response.status_code == 200, wrong_status_response_log(200, response)
    assert json.loads(response.data) == {
        "json_schema": {},
        "version": 0
    }


def test_GetSchema_WithData(db, client, data_schema1, data_schema1_data):
    response = client.get("/schema")
    assert response.status_code == 200, wrong_status_response_log(200, response)
    assert json.loads(response.data) == {
        "json_schema": data_schema1_data["json_schema"],
        "version": data_schema1_data["version"]
    }


def test_GetSchema_WithData_Version_Existing(db, client, data_schema1, data_schema1_data):
    response = client.get("/schema/version/0")
    assert response.status_code == 200, wrong_status_response_log(200, response)
    assert json.loads(response.data) == {
        "json_schema": data_schema1_data["json_schema"],
        "version": data_schema1_data["version"]
    }


def test_GetSchema_WithData_Version_NonExisting(db, client, data_schema1, data_schema1_data):
    response = client.get("/schema/version/1")
    assert response.status_code == 409, wrong_status_response_log(409, response)


def test_PutSchema_Success(db, client, data_schema1, data_schema1_data):
    new_schema = {
        "type": "object",
        "properties": {
            "foos": {"type": "string"}
        },
        "required": ["foos"]
    }
    response = client.put("/schema", json={
        "json_schema": new_schema,
        "json_blob": {
            "foos": "noon"
        },
        "reason": "Change foo to foos"
    })
    assert response.status_code == 200, wrong_status_response_log(200, response)

    data_schema = DataSchema.query.order_by(DataSchema.version.desc()).all()
    assert len(data_schema) == 2
    data = Data.query.order_by(Data.version.desc()).all()
    assert len(data) == 1

    assert data_schema[0].json_schema == new_schema
    assert data_schema[0].version == 1
    assert data[0].schema_version == 1
    assert data[0].version == 1


def test_PutSchema_Failed(db, client, data_schema1, data_schema1_data):
    new_schema = {
        "type": "object",
        "properties": {
            "foos": {"type": "string"}
        },
        "required": ["foos"]
    }
    response = client.put("/schema", json={
        "json_schema": new_schema,
        "json_blob": {
            "foos": 0
        },
        "reason": "Change foo to foos"
    })
    assert response.status_code == 409, wrong_status_response_log(409, response)


def test_GetData_WithoutData(db, client):
    response = client.get("/data")
    assert response.status_code == 200, wrong_status_response_log(200, response)
    assert json.loads(response.data) == {
        "data": {},
        "version": 0,
        "schema_version": 0
    }


def test_GetData_WithData(db, client, data1):
    response = client.get("/data")
    assert response.status_code == 200, wrong_status_response_log(200, response)
    assert json.loads(response.data) == {
        "data": {
            "foo": "bar"
        },
        "version": 0,
        "schema_version": 0
    }


def test_GetData_WithData_Version_Existing(db, client, data1):
    response = client.get("/data/version/0")
    assert response.status_code == 200, wrong_status_response_log(200, response)
    assert json.loads(response.data) == {
        "data": {
            "foo": "bar"
        },
        "version": 0,
        "schema_version": 0
    }


def test_GetData_WithData_Version_NonExisting(db, client, data1):
    response = client.get("/data/version/1")
    assert response.status_code == 409, wrong_status_response_log(409, response)


def test_GetData_WithData_SchemaVersion_Existing(db, client, data1):
    response = client.get("/data/schema_version/0")
    assert response.status_code == 200, wrong_status_response_log(200, response)
    assert json.loads(response.data) == {
        "data": {
            "foo": "bar"
        },
        "version": 0,
        "schema_version": 0
    }


def test_GetData_WithData_SchemaVersion_NonExisting(db, client, data1):
    response = client.get("/data/schema_version/1")
    assert response.status_code == 409, wrong_status_response_log(409, response)


def test_PutData_Success(db, client, data1, data_schema1):
    response = client.put("/data", json={
        "json_blob": {
            "foo": "noon"
        },
        "reason": "Manual"
    })
    assert response.status_code == 200, wrong_status_response_log(200, response)

    data = Data.query.order_by(Data.version.desc()).all()
    assert len(data) == 2

    expected_data = {
        "version": 1,
        "schema_version": 0,
        "json_blob": {
            "foo": "noon"
        },
        "reason": "Manual"
    }
    assert data[0].json_blob == expected_data["json_blob"]
    assert data[0].version == expected_data["version"]
    assert data[0].schema_version == expected_data["schema_version"]
    assert data[0].reason == expected_data["reason"]


def test_PutData_MismatchedSchema(db, client, data1, data_schema1, data_schema2):
    # This error barely has a chance of existing as it can only happen in the event
    # that either the schema was updated without updating the data, or if the schema
    # gets updated between the endpoint querying for the latest data and the Validator
    # querying for the latest schema.
    response = client.put("/data", json={
        "json_blob": {
            "foos": "noon",
            "extra": "thing"
        },
        "reason": "Manual"
    })
    assert response.status_code == 409, wrong_status_response_log(409, response)

    data = Data.query.order_by(Data.version.desc()).all()
    assert len(data) == 1


def test_PutData_NotSchemaValid(db, client, data1, data_schema1):
    response = client.put("/data", json={
        "json_blob": {
            "foos": "noon",
            "extra": "thing"
        },
        "reason": "Manual"
    })
    assert response.status_code == 409, wrong_status_response_log(409, response)

    data = Data.query.order_by(Data.version.desc()).all()
    assert len(data) == 1


def test_DataReset_NoSchemaVersion_LatestDataVersion(db, client, data1, data_schema1):
    response = client.post("/data/reset")
    assert response.status_code == 409, wrong_status_response_log(409, response)


def test_DataReset_NoSchemaVersion(db, client, data1, data1_data, data_schema1, data2, data2_data):
    response = client.post("/data/reset")
    assert response.status_code == 200, wrong_status_response_log(200, response)
    assert json.loads(response.data) == {
        "data": data1_data["json_blob"],
        "version": data2_data["version"] + 1,
        "schema_version": data1_data["schema_version"]
    }


def test_DataReset_SpecifiedVersion_NotExisting(db, client, data1, data_schema1):
    response = client.post("/data/reset/1")
    assert response.status_code == 409, wrong_status_response_log(409, response)


def test_DataReset_SpecifiedVersion_CurrentSchema_Existing(db, client, data1, data1_data, data_schema1, data2, data2_data):
    response = client.post("/data/reset/0")
    assert response.status_code == 200, wrong_status_response_log(200, response)
    assert json.loads(response.data) == {
        "data": data1_data["json_blob"],
        "version": data2_data["version"] + 1,
        "schema_version": data1_data["schema_version"]
    }


def test_DataReset_SpecifiedVersion_OldSchema(db, client, data1, data1_data, data_schema1, data2, data3, data3_data, data_schema2, data_schema2_data):
    response = client.post("/data/reset/0")
    assert response.status_code == 200, wrong_status_response_log(200, response)
    assert json.loads(response.data) == {
        "data": data1_data["json_blob"],
        "version": data3_data["version"] + 1,
        "schema_version": data3_data["schema_version"] + 1
    }

    schema = DataSchema.query.order_by(DataSchema.version.desc()).all()
    assert schema[0].version == data3_data["schema_version"] + 1


def test_commit_update(db, client, data1, data1_data, data2_data, data_schema1):
    json_blob = SimpleNamespace(**data2_data)
    json_blob.version = 0
    r = d.commit_update(json_blob, "foo")

    json_blob.version = 1
    json_blob.reason = "foo updater"
    assert json_blob.json_blob == r


def test_DataUpdate_SchemaInvalidation(db, client, data1, data_schema1):
    response = client.post("/update/foo", json={"data": 0})
    assert response.status_code == 409, wrong_status_response_log(409, response)


def test_DataUpdate(db, client, data1, data1_data, data2_data, data_schema1):
    response = client.post("/update/foo", json={"data": data2_data["json_blob"]["foo"]})
    assert response.status_code == 200, wrong_status_response_log(200, response)
    assert json.loads(response.data) == data2_data["json_blob"]

    data = Data.query.order_by(Data.version.desc()).all()
    assert data[0].version == data1_data["schema_version"] + 1


def test_GetHistory_Nothing(db, client):
    response = client.get("/data/history/10/0")
    assert response.status_code == 200, wrong_status_response_log(200, response)
    assert json.loads(response.data) == {}


def test_GetHistory_1Item(db, client, data1, data2, data2_data, data_schema1):
    response = client.get("/data/history/10/0")
    assert response.status_code == 200, wrong_status_response_log(200, response)
    assert json.loads(response.data) == {
        "history": [
            {
                "added": [],
                "removed": [],
                "updated": [
                    {"key": "foo", "old_value": "bar", "new_value": "baro"}
                ],
                "version": 1,
                "schema_version": 0,
                "reason": data2_data["reason"]
            }
        ],
        "pages": 1
    }
