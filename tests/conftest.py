import pytest
from app import create_app
from models import Data, DataSchema
from sqlalchemy_utils import drop_database
from sqlalchemy_utils.functions import create_database, database_exists


@pytest.fixture(scope='session')
def app():
    app = create_app()
    return app


@pytest.fixture(scope='session')
def appctx(app):
    # Generate app context as a fixture
    # App context is required for creating/dropping the database
    from models import db as db_
    with app.app_context():
        yield app
        # At the end of each session, delete the database file pyhsically
        drop_database(str(db_.engine.url))


@pytest.fixture(scope="session")
def client(app):
    return app.test_client()


@pytest.fixture(scope="function")
def db(appctx):
    # Generate database session as a fixture
    # Flush the database at the end of each module
    from models import db as db_
    if not database_exists(str(db_.engine.url)):
        create_database(str(db_.engine.url))
    db_.create_all()

    yield db_
    # At the end of each module, remove the session, drop all the tables and recreate them
    db_.session.remove()
    db_.drop_all()


@pytest.fixture(scope="module")
def data1_data():
    data1_data = {
        "version": 0,
        "schema_version": 0,
        "json_blob": {
            "foo": "bar"
        },
        "reason": "Initial"

    }
    return data1_data


@pytest.fixture(scope="function")
def data1(db, data1_data):
    data = Data(**data1_data)
    db.session.add(data)
    db.session.commit()
    return data


@pytest.fixture(scope="module")
def data2_data():
    data2_data = {
        "version": 1,
        "schema_version": 0,
        "json_blob": {
            "foo": "baro"
        },
        "reason": "Updated"

    }
    return data2_data


@pytest.fixture(scope="function")
def data2(db, data2_data):
    data = Data(**data2_data)
    db.session.add(data)
    db.session.commit()
    return data


@pytest.fixture(scope="module")
def data3_data():
    data3_data = {
        "version": 2,
        "schema_version": 1,
        "json_blob": {
            "foos": "bar"
        },
        "reason": "Initial"

    }
    return data3_data


@pytest.fixture(scope="function")
def data3(db, data3_data):
    data = Data(**data3_data)
    db.session.add(data)
    db.session.commit()
    return data


@pytest.fixture(scope="module")
def data_schema1_data():
    data_schema1_data = {
        "version": 0,
        "json_schema": {
            "type": "object",
            "properties": {
                "foo": {"type": "string"}
            },
            "required": ["foo"]
        },
        "reason": "Initial"

    }
    return data_schema1_data


@pytest.fixture(scope="function")
def data_schema1(db, data_schema1_data):
    data = DataSchema(**data_schema1_data)
    db.session.add(data)
    db.session.commit()
    return data


@pytest.fixture(scope="module")
def data_schema2_data():
    data_schema2_data = {
        "version": 1,
        "json_schema": {
            "type": "object",
            "properties": {
                "foos": {"type": "string"}
            },
            "required": ["foos"]
        },
        "reason": "Initial"

    }
    return data_schema2_data


@pytest.fixture(scope="function")
def data_schema2(db, data_schema2_data):
    data = DataSchema(**data_schema2_data)
    db.session.add(data)
    db.session.commit()
    return data


@pytest.fixture(scope="function")
def test_data():
    testdict = {
        "path": "one",
        "other": {
            "inside": 0,
            "paths": "string"
        },
        "dict": {
            "one": 1,
            "two": 2
        },
        "array": ["one", "two"]
    }
    return testdict
