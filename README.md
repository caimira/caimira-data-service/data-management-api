# CAiMIRA Data Service Management API

This is the REST API service that communicates with the database.

## How to get data
This now requires Azure AD authentication. We will not document the process of acquiring the JWT token here, for extra information please refer to the wiki pages covering our Azure AD integration.

## Installation

```bash
# Create a new virtual environment
[user@host]$ python3  -m venv venv

# Activate the virtual environment
[user@host]$ source venv/bin/activate

# Install dependencies
(venv) [user@host]$ pip install -r requirements.txt
```

## Running the updater
```bash
(venv) [user@host]$ flask run
```

## Running in a container using podman
```bash
# Start the VM
podman machine start
# Build the image
podman build -t managementapi:latest .
# Run the image
podman run --name managementapi managementapi:latest
```

If there is need to stop and re-run the image:
```bash
# Stop the container
podman stop managementapi
# Remove the container
podman rm managementapi
# Run the image
podman run --name managementapi -p 5000:5000 managementapi:latest
```

If you need to rebuild the image after modification of code or the `Dockerfile`:
```bash
# Stop the container
podman stop managementapi
# Remove the container
podman rm managementapi
# Remove the image
podman image rm managementapi
# Build the new image
podman build -t managementapi:latest .
# Run the new image
podman run --name managementapi -p 5000:5000 managementapi:latest
```

To open a shell into the container, with the container running:

```
podman exec -it managementapi /bin/bash
```
