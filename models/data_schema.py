import uuid

from sqlalchemy_utils import UUIDType, JSONType

from .base import db


class DataSchema(db.Model):
    """
    This model stores the expected JSON schema with a version.
    """

    __tablename__ = "data_schema"

    id = db.Column(UUIDType, default=uuid.uuid4, primary_key=True)
    version = db.Column(db.Integer, unique=True)
    json_schema = db.Column(JSONType, nullable=False)
    reason = db.Column(db.String(128))

    def __repr__(self):
        return f"<data_schema {self.version}>"
