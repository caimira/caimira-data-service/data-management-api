import sqlalchemy as sa
from flask_sqlalchemy import Model as BaseModel
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy import MetaData, inspect
from sqlalchemy_utils import Timestamp, force_auto_coercion


class UpdateMixin:
    def update(self, **kwargs):
        for key, value in kwargs.items():
            setattr(self, key, value)
        return self


class TimestampMixin(Timestamp):
    deleted = sa.Column(sa.DateTime)


class Model(BaseModel, UpdateMixin, TimestampMixin):
    pass


convention = {
    "ix": "ix_%(column_0_label)s",
    "uq": "uq_%(table_name)s_%(column_0_name)s",
    "ck": "ck_%(table_name)s_%(constraint_name)s",
    "fk": "fk_%(table_name)s_%(column_0_name)s_%(referred_table_name)s",
    "pk": "pk_%(table_name)s",
}

metadata = MetaData(naming_convention=convention)
db = SQLAlchemy(model_class=Model, metadata=metadata)

# For password field to work
force_auto_coercion()


def get_all_tables(db):
    """Return a dict containing all tables grouped by schema."""
    inspector = inspect(db.engine)
    schemas = sorted(set(inspector.get_schema_names()) - {"information_schema"})
    return dict(
        zip(schemas, (inspector.get_table_names(schema=schema) for schema in schemas))
    )
