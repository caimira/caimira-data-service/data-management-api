import uuid

from sqlalchemy_utils import UUIDType, JSONType

from .base import db


class Data(db.Model):
    """
    This model stores the data collected by the updater with a version.
    This gives us a history of the parameters.
    """

    __tablename__ = "data"

    id = db.Column(UUIDType, default=uuid.uuid4, primary_key=True)
    version = db.Column(db.Integer, unique=True)
    schema_version = db.Column(db.Integer, nullable=False)
    json_blob = db.Column(JSONType, nullable=False)
    reason = db.Column(db.String(128))

    def __repr__(self):
        return f"<data {self.version}>"
